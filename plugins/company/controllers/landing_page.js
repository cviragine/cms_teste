﻿module.exports = function (pb) {

    //PB dependencies
    var util = pb.util;
    var BaseController = pb.BaseController;

    //PB dependencies
    var PluginService = new pb.PluginService;

    // Instantiate the controller & extend from the base controller
    var LandingPage = function () { };
    util.inherits(LandingPage, pb.BaseController);


  

    // Define the content to be rendered
    LandingPage.prototype.render = function (cb) {
        var self = this;
        var output = {
            content_type: 'text/html',
            code: 200
        };
        //this.ts.load('landing_page', function (error, result) {
        //    output.content = result;
        //    cb(output);
        //});

        var pluginService = new pb.PluginService;
       // var pluginService = new PluginService();
        pluginService.getSetting('landing_page_headline', 'company', function (err, headline) {
            //Load the subline setting
            pluginService.getSetting('landing_page_subheader', 'company', function (err, subheader) {
                //Register the custom directives
                self.ts.registerLocal('landing_page_headline', headline);
                self.ts.registerLocal('landing_page_subheader', subheader);
                self.ts.load('landing_page', function (error, result) {
                    output.content = result;
                    cb(output);
                });
            });
        });
    };

    // Define the routes for the controller
    LandingPage.getRoutes = function (cb) {
        var routes = [{
            method: 'get',
            path: "/",
            auth_required: false,
            content_type: 'text/html'
        }];
        cb(null, routes);
    };

    //return the prototype
    return LandingPage;
};